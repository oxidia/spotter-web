FROM ubuntu:20.04

RUN apt-get update --fix-missing -y

# install nodejs
RUN apt-get install curl -y
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update --fix-missing -y
RUN apt-get install -y nodejs

WORKDIR /app

CMD npm i && npm run dev
