import Head from "next/head";
import Link from "next/link";
import Layout from "../components/Layout";

function Custom404() {
  return (
    <Layout footerFixed>
      <Head>
        <title>Spotter | 404</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div
        className={"container h-screen flex items-center justify-center m-auto"}
      >
        <div className={"text-center"}>
          <h1 className={"text-8xl"}>
            <label className={"border-r-4 animate-pulse border-primary"}>
              404
            </label>
          </h1>
          <h3 className={"my-8 text-md"}>Page Not Found</h3>
          <h3 className={"text-xl"}>
            Oops are you lost? go{" "}
            <Link href="/">
              <a className={"text-blue-500"}>Home</a>
            </Link>
          </h3>
        </div>
      </div>
    </Layout>
  );
}

export default Custom404;
