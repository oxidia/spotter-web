import Head from "next/head";
import Layout from "../components/Layout";

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>Spotter | Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={"container m-auto text-center mt-40"}>
        <img
          className={"w-64 m-auto mb-10"}
          src={"/images/logo.png"}
          alt={"spotter-logo"}
        />

        <div className={"max-w-xl m-auto text-primary"}>
          <section className={"mb-20"}>
            <h3 className={"text-3xl font-bold m-auto mb-2"}>
              Our mission is to increase the ability to sell of our Users
            </h3>
            <p>
              <span className={"font-bold"}>Spotter</span> is a technology
              startup that has the ambition to enable businesses of every size –
              from new startups to big companies – to assess instantly the
              creditworthiness of their customers paying by{" "}
              <span className={"font-bold"}>cheques</span>.
            </p>
          </section>

          <section className={"mb-20"}>
            <img
              className={"h-48 m-auto"}
              src={"/images/connected-world.svg"}
            />
            <p className={"mt-5"}>
              <span className={"font-bold"}>Spotter</span> is committed to build
              a strong working relationship with the Moroccan banking sector in
              order to create an innovative service.
            </p>
          </section>

          <section className={"mb-20"}>
            <img className={"h-48 m-auto"} src={"/images/api.svg"} />
            <p className={"mt-5"}>
              <span className={"font-bold"}>Spotter</span> will be building its
              services around secured APIs, which are one piece of the whole
              open banking ecosystem, in order to help its users to quickly
              distinguish between a serious customer paying by a{" "}
              <span className={"font-bold"}>cheque</span> and another customer
              for whom the transaction won’t go through.
            </p>
          </section>

          <section className={"mb-20"}>
            <img className={"h-48 m-auto"} src={"/images/mobile.svg"} />
            <p className={"mt-5"}>
              <span className={"font-bold"}>Spotter</span> Spotter web and
              mobile application will be offering businesses an instant
              visibility based on accurate banking data in order to counter any{" "}
              <span className={"font-bold"}>cheque fraud</span> possibilities.
            </p>
          </section>
        </div>
      </div>
    </Layout>
  );
}
