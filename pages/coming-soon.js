import React from "react";
import Head from "next/head";

import Layout from "../components/Layout";

function ComingSoon() {
  return (
    <Layout footerFixed>
      <Head>
        <title>Spotter | Coming Soon</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={"container m-auto text-center mt-60"}>
        <img
          className={"w-64 m-auto mb-10"}
          src={"/images/logo.png"}
          alt={"spotter-logo"}
        />
        <div className={"max-w-5xl m-auto"}>
          <h1 className={"text-4xl mb-3"}>Web site under construction</h1>
          <h3 className={"text-3xl font-light"}>Coming soon</h3>
        </div>
      </div>
    </Layout>
  );
}

export default ComingSoon;
