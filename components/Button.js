import { forwardRef } from "react";

function Button(props, ref) {
  var { className, children, ...rest } = props;
  const tailwindStyle =
    "relative py-2 px-4 font-medium rounded-md text-white bg-primary hover:bg-primary-500";

  return (
    <button ref={ref} className={`${tailwindStyle} ${className}`} {...rest}>
      {children}
    </button>
  );
}

export default forwardRef(Button);
