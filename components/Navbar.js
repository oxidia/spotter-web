import Link from "next/link";

function Navbar(props) {
  var { children } = props;

  const tailwindContainerStyle =
    "flex fixed w-full top-0 h-20 shadow-lg bg-gray-100 z-10";
  const tailwindUlStyle = "flex relative h-20 items-center justify-start";

  return (
    <header className={tailwindContainerStyle}>
      <nav className={"container m-auto"}>
        <ul className={tailwindUlStyle}>
          <NavbarItem>
            <div>
              <Link href={"/"}>
                <img
                  className={"h-10 mt-2 cursor-pointer"}
                  src={"/images/logo.png"}
                  alt={"spotter logo"}
                />
              </Link>
            </div>
          </NavbarItem>
          {children}
        </ul>
      </nav>
    </header>
  );
}

function NavbarItem(props) {
  var { children } = props;

  var tailwindStyle = "px-5 py-1 text-indigo-900 hover:text-indigo-500";

  return <li className={tailwindStyle}>{children}</li>;
}

function ItemSeparator() {
  return <div className={"flex-grow"}></div>;
}

export { Navbar, NavbarItem, ItemSeparator };
