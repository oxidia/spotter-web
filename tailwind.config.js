var colors = require("tailwindcss/colors");

module.exports = {
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      primary: "#424770",
      "primary-500": "#333754",
      ...colors
    }
  },
  variants: {},
  purge: [
    // Use *.tsx if using TypeScript
    "./pages/**/*.js",
    "./components/**/*.js"
  ]
};
